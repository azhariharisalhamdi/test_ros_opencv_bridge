#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/video/background_segm.hpp"


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>


using namespace cv;

static const std::string OPENCV_WINDOW = "kamera";
static const std::string OPENCV_WINDOW_1 = "segmentasi";

class segmenros
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

public:
  segmenros()
    : it_(nh_)
  {
    // Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/usb_cam/image_raw", 1,
      &segmenros::imageCb, this);
    image_pub_ = it_.advertise("/segmen/raw_image", 1);
    cv::namedWindow(OPENCV_WINDOW);

  }

  ~segmenros()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {

    cv_bridge::CvImagePtr cv_ptr;
    namespace enc = sensor_msgs::image_encodings;

    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // Draw an example circle on the video stream
    if (cv_ptr->image.rows > 400 && cv_ptr->image.cols > 600){

	segmen_(cv_ptr->image);
    	image_pub_.publish(cv_ptr->toImageMsg());

	}
  }

  static void refineSegments(const Mat& img, Mat& mask, Mat& dst)
  {
    int niters = 3;

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    Mat temp;

    dilate(mask, temp, Mat(), Point(-1,-1), niters);
    erode(temp, temp, Mat(), Point(-1,-1), niters*2);
    dilate(temp, temp, Mat(), Point(-1,-1), niters);

    findContours( temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

    dst = Mat::zeros(img.size(), CV_8UC3);

    if( contours.size() == 0 )
        return;

    // iterate through all the top-level contours,
    // draw each connected component with its own random color
    int idx = 0, largestComp = 0;
    double maxArea = 0;

    for( ; idx >= 0; idx = hierarchy[idx][0] )
    {
        const vector<Point>& c = contours[idx];
        double area = fabs(contourArea(Mat(c)));
        if( area > maxArea )
        {
            maxArea = area;
            largestComp = idx;
        }
    }
    Scalar color( 0, 0, 255 );
    drawContours( dst, contours, largestComp, color, CV_FILLED, 8, hierarchy );
  }

  void segmen_(cv::Mat img)
  {
	bool update_bg_model = true;
	cv::Mat bgmask, out_frame;
	//cv::imshow(OPENCV_WINDOW,// )
	//cv::imshow(OPENCV_WINDOW_1,// )
	BackgroundSubtractorMOG bgsubtractor;
    	bgsubtractor.set("noiseSigma", 10);
	bgsubtractor(img, bgmask, update_bg_model ? -1 : 0);
	refineSegments(img, bgmask, out_frame);
	out_frame = cv::Scalar::all(0);
	cv::imshow(OPENCV_WINDOW,img);
        cv::imshow(OPENCV_WINDOW_1,out_frame );
  }



/*
  void detect_edges(cv::Mat img)
  {

   	cv::Mat src, src_gray;
	cv::Mat dst, detected_edges;

	int edgeThresh = 1;
	int lowThreshold = 200;
	int highThreshold =300;
	int kernel_size = 5;

	img.copyTo(src);

	cv::cvtColor( img, src_gray, CV_BGR2GRAY );
        cv::blur( src_gray, detected_edges, cv::Size(5,5) );
	cv::Canny( detected_edges, detected_edges, lowThreshold, highThreshold, kernel_size );

  	dst = cv::Scalar::all(0);
  	img.copyTo( dst, detected_edges);
	dst.copyTo(img);

    	cv::imshow(OPENCV_WINDOW, src);
    	cv::imshow(OPENCV_WINDOW_1, dst);
    	cv::waitKey(3);

  }
*/

  
};

int main(int argc, char** argv)
{
  bool update_bg_model = true;
  cv::Mat tmp_frame, bgmask, out_frame;
  BackgroundSubtractorMOG bgsubtractor;
  bgsubtractor.set("noiseSigma", 10);

  ros::init(argc, argv, "Edge_Detector");
  segmenros ic;
  ros::spin();
  return 0;
}
