#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <wiringSerial.h>

void gerak();

int main ()
{
  int fd ;

  if ((fd = serialOpen ("/dev/ttyAMA0", 9600)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

// Loop, getting and printing characters


  for (;;)
  {
	//serialPutchar (fd, gerak(0, )) ;
	serialPutchar(fd, gerak(0, 2550, 50));
  	serialPutchar(fd, gerak(1, 1250, 50));
  	serialPutchar(fd, gerak(2, 250, 50));
  	serialPutchar(fd, gerak(3, 1150, 50));
  	serialPutchar(fd, gerak(4, 1650, 50));
  	serialPutchar(fd, gerak(4, 2550, 20));
  	delay(1000);
  }
}



void gerak(int servo, int posisi, int waktu) {
   Serial.print("#");
   Serial.print(servo);
   Serial.print(" P");
   Serial.print(posisi);
   Serial.print(" T");
   Serial.println(waktu);

}
