#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/contrib/contrib.hpp"
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace cv;
using namespace std;

int window_size = 9;
int temp1;
int number_of_disparities = 80;
int temp2;
int pre_filter_size = 5;
int temp3;
int pre_filter_cap = 23;
int temp4;
int min_disparity = 30;
int temp5;
int texture_threshold = 500;
int temp6;
int uniqueness_ratio = 0;
int temp7;
int max_diff = 100;
float temp8;
int speckle_window_size = 0;
int temp9;




int main(int argc, char** argv[])
{
    cv::Mat frame; cv::Mat frame1;
    Mat framebaru1, framebaru, disparitiakhir, dispariti;

    VideoCapture cap1(1);
    VideoCapture cap2(2);



	int i1;
	int i2;
	int i3;
	int i4;
	int i5;
	int i6;
	int i7;
	int i8;
	int i9;
	namedWindow("dispariti");
	createTrackbar("WindowSize", "dispariti", &window_size,255, NULL);
	createTrackbar("no_of_disparities", "dispariti", &number_of_disparities,255, NULL);
	createTrackbar("filter_size", "dispariti", &pre_filter_size,255, NULL);
	createTrackbar("filter_cap", "dispariti", &pre_filter_cap,63, NULL);
	createTrackbar("min_disparity", "dispariti", &min_disparity,60, NULL);
	createTrackbar("texture_thresh", "dispariti", &texture_threshold,2000, NULL);
	createTrackbar("uniquness", "dispariti", &uniqueness_ratio,30, NULL);
	createTrackbar("disp12MaxDiff", "dispariti", &max_diff,100, NULL);
	createTrackbar("Speckle Window", "dispariti", &speckle_window_size,5000, NULL);

   while(1)
	{

		cap1 >> frame;
        cap2 >> frame1;

        cvtColor(frame, framebaru, CV_BGR2GRAY);
        cvtColor(frame1, framebaru1, CV_BGR2GRAY);

		i1 = window_size;
        StereoBM stereocam;
		if(i1%2==0 && i1>=7)
		{
			temp1 = i1-1;
			stereocam.state->SADWindowSize = temp1;
		}
		if(i1<7)
		{
			temp1 =	7;
			stereocam.state->SADWindowSize = temp1;
		}
	if(i1%2!=0 && i1>=7)
        {
		temp1 =	i1;
		stereocam.state->SADWindowSize = temp1;
	}


	i2 = number_of_disparities;
	if(i2%16!=0 && i2>16)
	{
		temp2 = i2 - i2%16;
		stereocam.state->numberOfDisparities = temp2;
	}
	if(i2%16==0 && i2>16)
    {
		temp2 =	i2;
		stereocam.state->numberOfDisparities = temp2;
	}
	if(i2<=16)
	{
		temp2 =	16;
		stereocam.state->numberOfDisparities = temp2;

	}


	i3 = pre_filter_size;
	if(i3%2==0 && i3>=7)
	{
		temp3 = i3-1;
		stereocam.state->preFilterSize = temp3;
	}
	if(i3<7)
	{
		temp3 =	7;
		stereocam.state->preFilterSize = temp3;

	}
	if(i3%2!=0 && i3>=7)
    {
		temp3 =	i3;
		stereocam.state->preFilterSize = temp3;
	}


	i4 = pre_filter_cap;
	if(i4>0)
	{
		temp4 = i4;
		stereocam.state->preFilterCap = temp4;
	}
	if(i4==0)
	{
		temp4 = 1;
		stereocam.state->preFilterCap = temp4;
	}


	i5 = min_disparity;
	temp5 = -i5;
	stereocam.state->minDisparity = temp5;



	i6 = texture_threshold;
	temp6 = i6;
	stereocam.state->textureThreshold = temp6;


	i7 = uniqueness_ratio;
	temp7 = i7;
	stereocam.state->uniquenessRatio = temp7;


	i8 = max_diff;
	temp8 = 0.01*((float)i8);
	stereocam.state->disp12MaxDiff = temp8;


	i9 = speckle_window_size;
	temp9 = i9;
	stereocam.state->speckleWindowSize = temp9;

	stereocam.state->speckleRange = 8;

    stereocam(framebaru, framebaru1, dispariti);
    normalize(dispariti, disparitiakhir, 0, 255, CV_MINMAX, CV_8U);
    imshow("xcxcsfsdfd", frame);
    imshow("dfffsdfsdf", frame1);
    imshow("dispariti", disparitiakhir);
	waitKey(1);
}
    return(0);
}
