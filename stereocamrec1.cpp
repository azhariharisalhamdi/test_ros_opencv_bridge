#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

int minHessian;
float fokus = 0.03, jarakkamera = 15.00, jarakobjek;


cv::Mat frame; cv::Mat frame1;
Mat framebaru1, framebaru, disparitiakhir, dispariti;
float scale=1.3;

/** @function main */
int main( int argc, char** argv )
{
 //namedWindow("keypoint", 1);
  VideoCapture cap1(1);
  VideoCapture cap2(0);
  cap1.set(CV_CAP_PROP_FRAME_WIDTH, 600);
  cap1.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
  cap2.set(CV_CAP_PROP_FRAME_WIDTH, 600);
  cap2.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
  //int minHessian = 10;
  Mat map11, map12, map21, map22;
  Mat img1r, img2r;

  string instereo = "intrinsics.yml";  //path ke file kalibrasi intrinsik
  string exstereo = "extrinsics.yml";  // path ke file kalibrasi extrinsik
  //string kamera2 = "right.yaml";
  FileStorage fs(instereo, FileStorage::READ);
  fs.open(instereo, FileStorage::READ);
  if(!fs.isOpened())
  {
    printf("Failed to open file\n");
    return -1;
  }
  Mat M1, D1, M2, D2;
  fs["M1"] >> M1;
  fs["D1"] >> D1;
  fs["M2"] >> M2;
  fs["D2"] >> D2;
  M1 *= scale;
  M2 *= scale;
  Rect roi1, roi2;
  Mat Q;
  fs.open(exstereo, FileStorage::READ);
  Mat R, T, R1, P1, R2, P2;
  fs["R"] >> R;
  fs["T"] >> T;
  //fs["R1"] >> R1;
  //fs["R2"] >> R2;
  //fs["P1"] >> P1;
  //fs["P2"] >> P2;
  //fs["Q"] >> Q;
  stereoRectify( M1, D1, M2, D2, Size(600,600), R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, Size(600,600), &roi1, &roi2 );
  initUndistortRectifyMap(M1, D1, R1, P1, Size(600,600), CV_16SC2, map11, map12);
  initUndistortRectifyMap(M2, D2, R2, P2, Size(600,600), CV_16SC2, map21, map22);

/*
  stereoRectify( M1, D1, M2, D2, Size(600,600), R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, Size(600,600), &roi1, &roi2 );

  Mat map11, map12, map21, map22;
  initUndistortRectifyMap(M1, D1, R1, P1, Size(600,600), CV_16SC2, map11, map12);
  initUndistortRectifyMap(M2, D2, R2, P2, Size(600,600), CV_16SC2, map21, map22);

  Mat img1r, img2r;
  remap(frame, img1r, map11, map12, INTER_LINEAR);
  remap(frame1, img2r, map21, map22, INTER_LINEAR);

  frame = img1r;
  frame1 = img2r;
*/
    //IplImage*
    while(1) {

      cap1 >> frame;
      cap2 >> frame1;
      //stereoRectify( M1, D1, M2, D2, Size(600,600), R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, Size(600,600), &roi1, &roi2 );

      //Mat map11, map12, map21, map22;
//      initUndistortRectifyMap(M1, D1, R1, P1, Size(600,600), CV_16SC2, map11, map12);
//      initUndistortRectifyMap(M2, D2, R2, P2, Size(600,600), CV_16SC2, map21, map22);

      //Mat img1r, img2r;
      remap(frame, img1r, map11, map12, INTER_LINEAR);
      remap(frame1, img2r, map21, map22, INTER_LINEAR);

      frame = img1r;
      frame1 = img2r;	
        

	cvtColor(frame, framebaru, CV_BGR2GRAY);
        cvtColor(frame1, framebaru1, CV_BGR2GRAY);

        StereoBM stereocam;
        stereocam.state->roi1 = roi1;
        stereocam.state->roi2 = roi2;
        stereocam.state->SADWindowSize = 9;
        stereocam.state->numberOfDisparities = 80;
        stereocam.state->preFilterSize = 5;
        stereocam.state->preFilterCap = 61;
        stereocam.state->minDisparity = 30;
        stereocam.state->textureThreshold = 2000;
        stereocam.state->uniquenessRatio = 0;
        stereocam.state->speckleWindowSize = 0;
        stereocam.state->speckleRange = 4;
        stereocam.state->disp12MaxDiff = 0;

        stereocam(framebaru, framebaru1, dispariti);
        normalize(dispariti, disparitiakhir, 0, 255, CV_MINMAX, CV_8U);


        imshow("disp", disparitiakhir);
        imshow("dsdsdsdsd", frame);

        imshow("dsdsdsd", frame1);


        char c = cvWaitKey(33);
        if( c == 27 ) break;
    }

  waitKey(0);

  return 0;
  }
