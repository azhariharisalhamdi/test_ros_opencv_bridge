#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <stdio.h>
#include <iostream>

static const std::string opencvros = "kamera Raw";
static const std::string opencvros1 = "TES SURF OPENCV";

class Tes_SURF
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

  public:
    Tes_SURF()
    : it_(nh_)
    {
      // Subscribe to input video feed and publish output video feed
      image_sub_ = it_.subscribe("image raw", 1,
      &Tes_SURF::imageCb, this);
      image_pub_ = it_.advertise("surf raw image", 1);
      cv::namedWindow(opencvros);

    }

    ~Tes_SURF()
    {
      cv::destroyWindow(opencvros);
    }

    void imageCb(const sensor_msgs::ImageConstPtr& msg)
    {

      cv_bridge::CvImagePtr cv_ptr;
      namespace enc = sensor_msgs::image_encodings;

      try
      {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      }
      catch (cv_bridge::Exception& e)
      {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
      }

    // Draw an example circle on the video stream
      if (cv_ptr->image.rows > 400 && cv_ptr->image.cols > 600){

	      Hasil_SURF(cv_ptr->image);
    	  image_pub_.publish(cv_ptr->toImageMsg());

	     }
     }
  void Hasil_SURF(cv::Mat img)
  {

/*
      cv::Mat src, src_gray;
	    cv::Mat dst, detected_edges;

	    int edgeThresh = 1;
	    int lowThreshold = 200;
	    int highThreshold =300;
	    int kernel_size = 5;

	    img.copyTo(src);

	    cv::cvtColor( img, src_gray, CV_BGR2GRAY );
      cv::blur( src_gray, detected_edges, cv::Size(5,5) );
	    cv::Canny( detected_edges, detected_edges, lowThreshold, highThreshold, kernel_size );

  	  dst = cv::Scalar::all(0);
  	  img.copyTo( dst, detected_edges);
	    dst.copyTo(img);

    	cv::imshow(opencvros, src);
    	cv::imshow(opencvros1, dst);
    	cv::waitKey(3);
*/
      //using namespace cv;

      int minHessian;
      //VideoCapture cap(0);
      //VideoCapture cap2(2);

      cv::Mat frame; cv::Mat frame1; Mat edges; Mat surfraw;
      img.copyTo(frame);

      cv::SURF detector( minHessian );
      //Canny(frame, edges, 10, 90, 3);
      std::vector<KeyPoint> keypoints_1;
      detector.detect(frame, keypoints_1);

      drawKeypoints( frame, keypoints_1, surfraw, Scalar::all(-1), DrawMatchesFlags::DEFAULT );
      surfraw.copyTo(img);
      createTrackbar("Hessian set", opencvros1, &minHessian, 1000);
      imshow( opencvros1, surfraw );
      imshow(opencvros, frame);

  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "Tes_SURF");
  Tes_SURF ic;
  ros::spin();
  return 0;
}
