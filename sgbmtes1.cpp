#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

int minHessian;
float fokus = 0.03, jarakkamera = 15.00, jarakobjek;


cv::Mat frame; cv::Mat frame1;
Mat framebaru1, framebaru, disparitiakhir, dispariti;

/** @function main */
int main( int argc, char** argv )
{
 //namedWindow("keypoint", 1);
  VideoCapture cap1(0);
  VideoCapture cap2(1);
  cap1.set(CV_CAP_PROP_FRAME_WIDTH, 600);
  cap1.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
  cap2.set(CV_CAP_PROP_FRAME_WIDTH, 600);
  cap2.set(CV_CAP_PROP_FRAME_HEIGHT, 600);
  //int minHessian = 10;



    //IplImage*
    while(1) {
        cap1 >> frame;
        cap2 >> frame1;

        cvtColor(frame, framebaru, CV_BGR2GRAY);
        cvtColor(frame1, framebaru1, CV_BGR2GRAY);

        StereoBM stereocam;
        stereocam.state->SADWindowSize = 9;
        stereocam.state->numberOfDisparities = 64;
        stereocam.state->preFilterSize = 5;
        stereocam.state->preFilterCap = 61;
        stereocam.state->minDisparity = -39;
        stereocam.state->textureThreshold = 507;
        stereocam.state->uniquenessRatio = 0;
        stereocam.state->speckleWindowSize = 0;
        stereocam.state->speckleRange = 8;
        stereocam.state->disp12MaxDiff = 1;

        StereoSGBM sgbm;
        sgbm.SADWindowSize = 5;
        sgbm.numberOfDisparities = 192;
        sgbm.preFilterCap = 4;
        sgbm.minDisparity = -64;
        sgbm.uniquenessRatio = 1;
        sgbm.speckleWindowSize = 150;
        sgbm.speckleRange = 2;
        sgbm.disp12MaxDiff = 10;
        sgbm.fullDP = false;
        sgbm.P1 = 600;
        sgbm.P2 = 2400;

        sbm.state->SADWindowSize = 9;
        sbm.state->numberOfDisparities = 112;
        sbm.state->preFilterSize = 5;
        sbm.state->preFilterCap = 61;
        sbm.state->minDisparity = -39;
        sbm.state->textureThreshold = 507;
        sbm.state->uniquenessRatio = 0;
        sbm.state->speckleWindowSize = 0;
        sbm.state->speckleRange = 8;
        sbm.state->disp12MaxDiff = 1;

        //stereocam(framebaru, framebaru1, dispariti);
        sgbm(framebaru, framebaru1, dispariti);
        normalize(dispariti, disparitiakhir, 0, 255, CV_MINMAX, CV_8U);


        imshow("disp", disparitiakhir);
        imshow("dsdsdsdsd", frame);

        imshow("dsdsdsd", frame1);


        char c = cvWaitKey(33);
        if( c == 27 ) break;
    }

  waitKey(0);

  return 0;
}
